<?php

namespace App\Controller;

use App\Entity\Department;
use App\Entity\Student;
use App\Form\StudentType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StudentController extends AbstractController
{
    #[Route('/student-list', name: 'studentList')]
    public function index(): Response
    {
        return $this->render('student/index.html.twig', [
            'students'          => $this->getDoctrine()->getManager()->getRepository(Student::class)->findAll(),
            'controller_name'   => 'StudentController',
        ]);
    }

    #[Route('/student-creation', name: 'studentCreation')]
    public function create(Request $request): Response
    {
        $student = new Student();

        $form = $this->createForm(StudentType::class, $student);

        $form->handleRequest($request);
        
        if ($form->isSubmitted() === true && $form->isValid() === true) {
            $student = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();

            // Just for the example
            $department = new Department();
            $department->setName('Department 1');
            $department->setCapacity(8);

            $student->setDepartment($department);

            $entityManager->persist($student);
            $entityManager->persist($department);
            $entityManager->flush();

            dd(sprintf('Student %d created and department %d was attached !', $student->getId(), $department->getId()));
            
            return $this->redirectToRoute('studentList', [
                'department'    => $department->getId()
            ]);
        }

        return $this->renderForm('student/create.html.twig', [
            'form'  => $form
        ]);
    }

    #[Route('/student-edition/{id}', name: 'studentEdition')]
    public function edit(Request $request, $id): Response
    {
        $student = $this->getDoctrine()->getRepository(Student::class);
        $student = $student->find($id);

        if (!$student) {
            throw $this->createNotFoundException('Student with id ' . $id . ' doesn\'t exist');
        }

        $form = $this->createForm(StudentType::class, $student);

        $form->handleRequest($request);

        if ($form->isSubmitted() === true && $form->isValid() === true) {
            $student = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($student);
            $entityManager->flush();

            return $this->redirectToRoute('studentList', [
                'id'    => $student->getId()
            ]);
        }

        return $this->renderForm('student/edit.html.twig', [
            'form'  => $form
        ]);
    }

    #[Route('/student-deletion/{id}', name: 'studentDeletion')]
    public function delete($id): Response
    {
        $student = $this->getDoctrine()->getRepository(Student::class);
        $student = $student->find($id);

        if (!$student) {
            throw $this->createNotFoundException('Student with id ' . $id . ' doesn\'t exist');
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($student);
        $entityManager->flush();

        return $this->redirectToRoute('studentList', [
            'students'          => $this->getDoctrine()->getManager()->getRepository(Student::class)->findAll(),
            'controller_name'   => 'StudentController',
        ]);
    }
}
