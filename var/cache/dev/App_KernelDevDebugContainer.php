<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerXxVRG5t\App_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerXxVRG5t/App_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerXxVRG5t.legacy');

    return;
}

if (!\class_exists(App_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerXxVRG5t\App_KernelDevDebugContainer::class, App_KernelDevDebugContainer::class, false);
}

return new \ContainerXxVRG5t\App_KernelDevDebugContainer([
    'container.build_hash' => 'XxVRG5t',
    'container.build_id' => '79e89b3a',
    'container.build_time' => 1633022704,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerXxVRG5t');
