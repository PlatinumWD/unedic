<?php

namespace ContainerYtJVkr7;
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'persistence'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'Persistence'.\DIRECTORY_SEPARATOR.'ObjectManager.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).''.\DIRECTORY_SEPARATOR.'vendor'.\DIRECTORY_SEPARATOR.'doctrine'.\DIRECTORY_SEPARATOR.'orm'.\DIRECTORY_SEPARATOR.'lib'.\DIRECTORY_SEPARATOR.'Doctrine'.\DIRECTORY_SEPARATOR.'ORM'.\DIRECTORY_SEPARATOR.'EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder275fa = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializerf8c58 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicPropertiesa219d = [
        
    ];

    public function getConnection()
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'getConnection', array(), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'getMetadataFactory', array(), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'getExpressionBuilder', array(), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'beginTransaction', array(), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->beginTransaction();
    }

    public function getCache()
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'getCache', array(), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->getCache();
    }

    public function transactional($func)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'transactional', array('func' => $func), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->transactional($func);
    }

    public function commit()
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'commit', array(), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->commit();
    }

    public function rollback()
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'rollback', array(), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'getClassMetadata', array('className' => $className), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'createQuery', array('dql' => $dql), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'createNamedQuery', array('name' => $name), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'createQueryBuilder', array(), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'flush', array('entity' => $entity), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'clear', array('entityName' => $entityName), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->clear($entityName);
    }

    public function close()
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'close', array(), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->close();
    }

    public function persist($entity)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'persist', array('entity' => $entity), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'remove', array('entity' => $entity), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'refresh', array('entity' => $entity), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'detach', array('entity' => $entity), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'merge', array('entity' => $entity), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'getRepository', array('entityName' => $entityName), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'contains', array('entity' => $entity), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'getEventManager', array(), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'getConfiguration', array(), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'isOpen', array(), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'getUnitOfWork', array(), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'getProxyFactory', array(), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'initializeObject', array('obj' => $obj), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'getFilters', array(), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'isFiltersStateClean', array(), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'hasFilters', array(), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return $this->valueHolder275fa->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializerf8c58 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder275fa) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder275fa = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder275fa->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, '__get', ['name' => $name], $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        if (isset(self::$publicPropertiesa219d[$name])) {
            return $this->valueHolder275fa->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder275fa;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder275fa;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder275fa;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder275fa;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, '__isset', array('name' => $name), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder275fa;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder275fa;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, '__unset', array('name' => $name), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder275fa;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder275fa;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, '__clone', array(), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        $this->valueHolder275fa = clone $this->valueHolder275fa;
    }

    public function __sleep()
    {
        $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, '__sleep', array(), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;

        return array('valueHolder275fa');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializerf8c58 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializerf8c58;
    }

    public function initializeProxy() : bool
    {
        return $this->initializerf8c58 && ($this->initializerf8c58->__invoke($valueHolder275fa, $this, 'initializeProxy', array(), $this->initializerf8c58) || 1) && $this->valueHolder275fa = $valueHolder275fa;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder275fa;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder275fa;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
